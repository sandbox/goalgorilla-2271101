<?php

/**
 * @file
 * This module protects the admin account from being altered.
 */

use \Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Implements hook_user_presave().
 *
 * We check if the admin user is being edited and if we need to protect that
 * account.
 */
function gg_protect_user_presave($account) {
  // Get the path.
  $path = explode('?', request_uri());
  $path = explode('/', $path[0]);

  // Only act if Drush isn't involved. Also check for new site installation via
  // url.
  if (!drupal_is_cli() && !in_array('install.php', $path)) {
    $current_user = \Drupal::currentUser();

    // Is someone, other than user 1, trying to change user 1? Act!
    if ($account->id() == 1 && $account->id() != $current_user->id()) {
      // Set a variable which we will check at the end to see if we need to
      // protect the user.
      $protect = FALSE;

      // Deny changing the username.
      if ($account->getUsername() != $account->original->getUsername()) {
        $protect = TRUE;
      }

      // Deny changes to the e-mailaddress.
      if ($account->getEmail() != $account->original->getEmail()) {
        $protect = TRUE;
      }

      // Deny changes to the password.
      if ($account->getPassword() != $account->original->getPassword()) {
        $protect = TRUE;
      }

      // Protect user 1 from being blocked.
      if ($account->isBlocked()) {
        $protect = TRUE;
      }

      if ($protect === TRUE) {
        // Someone other than user 1 tries to cancel the account.
        drupal_set_message(t('You tried to edit the user 1 account, it is however protected. Please consult with user 1 about this matter. If you are sure this account should be edited, your best bet is through direct access to the database.'), 'warning');

        // Log this action.
        watchdog('protect_admin', '%user attempted to edit the user 1 account.', array('%user' => $current_user->getUsername()), WATCHDOG_WARNING);

        // Terminate the process by redirecting to a custom destination or
        // admin/people.
        $redirect = new RedirectResponse(url('admin/people'));
        $redirect->send();
        exit;
      }
    }
  }
}
